import React, { useState, useEffect } from 'react';
import { Table, Pagination } from 'react-bootstrap';
import axios from 'axios';
import Swal from 'sweetalert2'
import 'bootstrap/dist/css/bootstrap.min.css';


const TableFiles = () => {
  const URL_GET = "http://localhost:3000/files/data"
  const itemsPerPage = 5;
  const [currentPage, setCurrentPage] = useState(1);
  const [data, setData] = useState([]);
  const [getError, setError] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      try {
        Swal.fire(alertLoader);

        const response = await axios.get(URL_GET);;
        const result = await response.data;

        formattFile(result.data)

        Swal.close()
        Swal.fire(alertSuccess)

      } catch (error) {
        console.error('Error al obtener datos:', error);
        Swal.fire(alertError)
        catchError(error)
      }
    };

    fetchData();
  }, []);

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = data.slice(indexOfFirstItem, indexOfLastItem);

  const totalPages = Math.ceil(data.length / itemsPerPage);

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const alertSuccess = {
    icon: 'success',
    title: 'Modificado con Exito',
    showConfirmButton: false,
    timer: 1200
  }

  const alertError = {
    icon: 'error',
    title: 'Oops...',
    text: 'Something went wrong!',
  }

  const alertLoader = {
    title: 'Please Wait !',
    html: 'processing',
    allowOutsideClick: false,
    didOpen: () => {
      Swal.showLoading();
    }
  }

  const catchError = (axiosError) => {

    if (axiosError.response) {
      setError(`Error ${axiosError.response.status}: ${axiosError.response.data.error}`);
    } else if (axiosError.request) {
      setError('No se recibió respuesta del servidor');
    } else {
      setError('Error en la configuración de la solicitud');
    }
    console.log(getError)
  }

  const formattFile = (dataFile) => {
    const data = []
    dataFile.forEach(file => {
      file.lineas.forEach(row => {
        data.push({ name: file.files, text: row.text, number: row.number, hex: row.hex });
      });
    });

    setData(data);
  };

  return (
    <div className='table-files'>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>File Name</th>
            <th>Text</th>
            <th>Number</th>
            <th>Hex</th>
          </tr>
        </thead>
        <tbody>
          {currentItems.map((item) => (
            <tr key={item.hex}>
              <td>{item.name}</td>
              <td>{item.text}</td>
              <td>{item.number}</td>
              <td>{item.hex}</td>
            </tr>
          ))}
        </tbody>
      </Table>

      <Pagination>
        <Pagination.Prev
          onClick={() => handlePageChange(currentPage - 1)}
          disabled={currentPage === 1}
        />
        {Array.from({ length: totalPages }).map((_, index) => (
          <Pagination.Item
            key={index + 1}
            active={index + 1 === currentPage}
            onClick={() => handlePageChange(index + 1)}
          >
            {index + 1}
          </Pagination.Item>
        ))}
        <Pagination.Next
          onClick={() => handlePageChange(currentPage + 1)}
          disabled={currentPage === totalPages}
        />
      </Pagination>
    </div>
  );
};

export default TableFiles;
