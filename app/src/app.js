import React from "react";
import TableFiles from "./components/TableFiles";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

function App() {
  return (
    <div>
      <h3 className="title-file"> React Test App </h3>
      <Container>
        <Row>
          <TableFiles />
        </Row>
      </Container>
    </div>
  );
}

export default App;
