const express = require('express');
const cors = require('cors');

const FileRoutes = require('./routes/fileRoutes');

// Server Class
class Server {
    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    config() {
        // Settings
        this.app.set('port', 3000);

        // middlewares
        this.app.use(cors());
    }

    routes() {
        const router = express.Router();

        this.app.use('/files/data', FileRoutes);
    }

    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log('Server on port', this.app.get('port'));
        });
    }
}

module.exports = { Server };
