const axios = require('axios');

class FilesModel {

    constructor() { }

    async getFiles() {
        const url = 'https://echo-serv.tbxnet.com/v1/secret/files';
        const headers = {
            'accept': 'application/json',
            'authorization': 'Bearer aSuperSecretKey'
        };
        const response = await axios.get(url, { headers });
        const files = response.data.files
        // get all file for each name of file 
        const fileFormatted = await Promise.all(files.map(async (nameFile) => this.getFile(nameFile)));

        return fileFormatted

    }

    // get a only file 
    async getFile(nameFile) {
        const url = `https://echo-serv.tbxnet.com/v1/secret/file/${nameFile}`;
        const headers = {
            'accept': 'application/json',
            'authorization': 'Bearer aSuperSecretKey'
        };

        try {
            const response = await axios.get(url, { headers });
            const responseData = response.data;
            const format = await this.formatFile(responseData, nameFile);
            return format;
        } catch (error) {
            if (error.response) {
                console.log({ nameFile, status: error.response.status, statusText: error.response.statusText, data: error.response.data });
                return { files: nameFile, lineas: [] };
            }
        }
    }

    // formats and validates and cleans all files and then groups them into a single object
    async formatFile(file, fileName) {
        const lines = file.split('\n');
        const result = [];
        const newFile = { "files": fileName, "lineas": [] }

        const headers = lines[0].split(',');

        for (let i = 1; i < lines.length; i++) {
            const currentLine = lines[i].split(',');

            if (currentLine.length === headers.length && !currentLine.includes('')) {
                const obj = {};
                for (let j = 1; j < headers.length; j++) {
                    obj[headers[j].trim()] = currentLine[j].trim();
                }
                result.push(obj);
            }
        }
        newFile.lineas = result
        return newFile
    }

}

const model = new FilesModel();

module.exports = model;
