const { Request, Response } = require('express');
const FilesModel = require('../models/Files');

class FileController {
    constructor() { }

    async getFiles(req, res) {

        try {
            const responseData = await FilesModel.getFiles()
            res.status(200).json({
                data: responseData,
                message: 'Files'
            });
        } catch (error) {
            console.log("error", error)
            res.status(500).json({
                data: {},
                message: 'Error en la solicitud'
            });
        }
    }
}

const controller = new FileController();

module.exports = controller;
