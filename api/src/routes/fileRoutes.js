const { Router } = require('express');
const FileController = require('../controllers/FileController.js');

class FileRouter {
    constructor() {
        this.router = Router();
        this.routes();
    }

    routes() {
        this.router.get('/', FileController.getFiles);
    }
}

const fileRouter = new FileRouter();
module.exports = fileRouter.router;
