const chai = require('chai');
const expect = chai.expect;
const FileService = require('../src/models/Files');

describe('FileService', () => {

    it('It should bring all the files in an array', async () => {
        const files = await FileService.getFiles();
        expect(files).to.be.an('array');
        expect(files).to.have.lengthOf.above(0);
    });
});