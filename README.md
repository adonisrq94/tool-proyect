# Nombre del Proyecto

Descripción corta del proyecto.

## Estructura del Proyecto

- `app/`: Contiene la aplicación de frontend hecha en React con Webpack.
- `api/`: Contiene la aplicación del backend hecha en Node.js con Express.
- `docker-compose.yml`: Archivo de configuración de Docker Compose para la ejecución principal.
- `docker-compose.test.yml`: Archivo de configuración de Docker Compose para ejecutar pruebas en el backend.

## Instalación

### Requisitos Previos

- [Docker](https://www.docker.com/) instalado en tu máquina.

### Pasos de Instalación

1. **Clona el Repositorio:**

    ```bash
    git clone https://gitlab.com/adonisrq94/tool-proyect.git
    ```

# Uso de la API

La API proporciona endpoints para obtener información sobre los archivos y sus líneas. A continuación, se detallan los endpoints disponibles:

## Obtener Información de Archivos

### Endpoint

- **GET /files/data**

### Respuesta Exitosa

```json
{
  "data": [
    {
      "files": "test2.csv",
      "lineas": [
        {
          "text": "eOEmGrysBTcbQYoMYY",
          "number": "68156389633729309892752273074142",
          "hex": "7ef7e5d5876bdfcc61b703f84e674d3d"
        }
      ]
    },
    {
      "files": "test3.csv",
      "lineas": [
        {
          "text": "SeHssJeqjAyj",
          "number": "798871707",
          "hex": "8d296befc646807c72b8410434029616"
        },
        {
          "text": "Xi",
          "number": "0504",
          "hex": "0dc7ff07c0f6c98bf70be9afc1378827"
        },
        {
          "text": "vQxyLrHwryRnLFgDlNzbzXVJKmsAVf",
          "number": "727",
          "hex": "a914d838d1542a49b865270e4805c887"
        }
      ]
    }
  ],
  "message": "Files"
}
```

## Uso con Docker

1. **Ejecutar la Aplicación:**

    Para levantar tanto la aplicación de frontend como la API de backend, ejecuta:

    ```bash
    docker-compose up
    ```

    La aplicación estará disponible en `http://localhost:8000/`.

2. **Ejecutar Pruebas en el Backend:**

    Para ejecutar las pruebas en el backend, utiliza:

    ```bash
    docker-compose -f docker-compose.test.yml run test
    ```

![Texto Alternativo](./react_app.jpg)
